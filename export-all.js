var mysql = require('mysql');
var assert = require('assert');
var fs = require('fs');
var config = require('./config.json');
var figlet = require('figlet');
var locations = "(26,41,66,67,68,69,73,74,75,76,77,78,79)"; //"(72)";// To test a specific page in a fake program, create the page under location id 72 book id 199. export loc 72 and change a few pages in the normal pages.json file to have the id of the new pages. then import only the pages file you manually edited and the elements page from the export. 
//possible statuses: 'published','draft','unpublished','in-progress','ready for review','in-review'
var statuses = "('published','draft','unpublished','in-progress','ready for review','in-review')";

console.log(locations);

figlet('export all', function(err, data) {
  if (err) {
    console.log('Something went wrong...');
    console.dir(err);
    return;
  }
  console.log(data)

  // write the timestamp to the version file
  let versionStream = fs.createWriteStream(config.dumpPath + '/version.txt');
  versionStream.once('open', function(fd) {
    var datetime = require('node-datetime');
    var dt = datetime.create();
    var formatted = dt.format('m/d/Y H:M:S');
    console.log(formatted);
    versionStream.write(formatted);
    versionStream.end();
  });

  var castFields = function(field, useDefaultTypeCasting) {
    // Only cast bit fields that have a single-bit. If the field
    // has more than one bit, it cannot be assume it is a boolean.
    if (field.type === "BIT" && field.length === 1) {
      let bytes = field.buffer();
      // A Buffer in Node represents a collection of 8-bit unsigned integers.
      // Therefore, our single "bit field" comes back as the bits '0000 0001',
      // which is equivalent to the number 1.
      return bytes[0] === 1;
    } else if (field.type === "TINY") {
      // cast some field that are known to be booleans
      if (field.length === 1 || field.name === "teacher_only" || field.name === "protected" || field.name === "reviewer_access") {
//        console.log(field.type + "(" + field.length + ")", field.table + "." + field.name, field.string());
        return field.string() === '1';
      }
    } else if (field.name === "teacher_only") {
      // Field teacher_only is always a boolean regardless of its data type. This covers every occurrence.
//      console.log(field.type + "(" + field.length + ")", field.table + "." + field.name, field.string());
      return field.string() === '1';
    }
    return useDefaultTypeCasting();
  };

  let conn = mysql.createConnection({
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database,
    typeCast: castFields
  });
  console.log("connected to mysql");

  let bookQuery = `
    SELECT b.*, l.description location, l.abbr, l.program_name, l.series
    FROM book b JOIN locations l ON b.location_id = l.id
    WHERE b.book_type = 2 and l.id IN   ` + locations;

  conn.query(bookQuery, function(err, rows, fields) {
    if (!err) {
      let stream = fs.createWriteStream(config.dumpPath + "/books.json");
      stream.once('open', function(fd) {
        for (let i = 0; i < rows.length; i++) {
          let book = rows[i];
          let json = {
            "_id" : book.id,
            "title" : book.title,
            "description" : book.description,
            "locationId" : book.location_id,
            "basePath" : book.base_path,
            "publicationDate" : book.publication_date,
            "image" : book.image,
            "copyright" : book.copyright,
            "isbn" : book.isbn,
            "publisher" : book.publisher,
            "gradeLevelId" : book.grade_level_id,
            "color" : book.color,
            "bookType" : book.book_type,
            "magicBookId" : book.magic_book_id,
            "displayPrefix" : book.display_prefix,
            "location" : book.location,
            "locationAbbr" : book.abbr,
            "programName" : book.program_name,
            "series" : book.series
          };
          let jsonStr = JSON.stringify(json);
          stream.write(jsonStr + "\n");
        }
        stream.end();
        console.log("books: " + rows.length);
      });
    } else {
      console.log('Error fetching books.');
    }
  });

  let chapterMap = {};
 
  let chapterQuery = `
    SELECT c.* FROM chapters c JOIN book b ON c.book_id = b.id WHERE b.book_type = 2 and b.location_id IN ` + locations;
  conn.query(chapterQuery, function(err, rows, fields) {
    if (!err) {
	  //commented 6/14
      //let stream = fs.createWriteStream(config.dumpPath + "/chapters.json");
	  //end comment
      //stream.once('open', function(fd) {
        for (let i = 0; i < rows.length; i++) {
          let chapter = rows[i];
          let chapjson = {
            "_id" : chapter.id,
            "title" : chapter.title,
            "number" : chapter.number,
            "label" : chapter.label,
            "sortOrder" : chapter.sort_order,
            "updatedAt" : chapter.updated_at,
            "startPage" : chapter.start_page,
            "endPage" : chapter.end_page,
            "bookId" : chapter.book_id,
            "itemTypeId" : chapter.item_type_id,
            "teacherOnly" : chapter.teacher_only,
			"tools" : []
          };
          /*commented this 6/14
		  let jsonStr = JSON.stringify(json);
          stream.write(jsonStr + "\n");
		  */
		  //build chapterMap here: 
		  chapterMap[chapter.id] = chapjson;		  		  
        }
		/*commented this 6/14
        stream.end();
		*/
        console.log("chapters: " + rows.length);
		
		//added 6/14
		let toolConn = mysql.createConnection({
          host: config.host,
          port: config.port,
          user: config.user,
          password: config.password,
          database: config.database,
          typeCast: castFields
		});

		let procedureCall = `CALL get_chapter_math_tools`;
		toolConn.query(procedureCall, function(err){
			if(err){
			console.log("Error with tool procedure call");
			}
		});
		let toolQuery = `SELECT * FROM math_tools_temp ORDER BY chapter_id`;
		//SELECT * FROM math_tools_temp ORDER BY chapter_id;
		toolConn.query(toolQuery, function(err, toolrows, fields) {
			if (!err) {
				for (let i = 0; i < toolrows.length; i++) {

					let data = toolrows[i];
					if(chapterMap[data.chapter_id] == undefined) continue;
					let chap = chapterMap[data.chapter_id];
					//console.log(data);
		
					let tool = {
						//"chapterId" : data.chapter_id,
						"url" : data.url,
						"name" : data.tool_name,
						"icon" : data.icon_path
					};
					chap.tools.push(tool);
				}
			  // All chapters and tools have been fetched now so write out the file.
              let stream = fs.createWriteStream(config.dumpPath + "/chapters.json");
              stream.once('open', function(fd) {
                for (key in chapterMap) {
                  let chapter = chapterMap[key];
                  let jsonStr = JSON.stringify(chapter);
                  stream.write(jsonStr + "\n");
                }
                stream.end();
              });
			  toolConn.end();
			}else {
				console.log('Error fetching tools', err);
			}
		//end added 6/14
		
      });
	  
    } else {
      console.log('Error fetching chapters');
    }
  });

  let sectionQuery = `
    SELECT s.* FROM sections s JOIN chapters c ON s.chapter_id = c.id
    JOIN book b ON c.book_id = b.id WHERE b.book_type = 2 and b.location_id IN ` + locations;
  conn.query(sectionQuery, function(err, rows, fields) {
    if (!err) {
      let stream = fs.createWriteStream(config.dumpPath + "/sections.json");
      stream.once('open', function(fd) {
        for (let i = 0; i < rows.length; i++) {
          let section = rows[i];
      let stds;
      if(section.standards == null) {
        stds = "";
      } else {
        stds = section.standards.split(',');
      }
		  //let stds = JSON.stringify(stdstr);
          let json = {
            "_id" : section.id,
            "title" : section.title,
            "number" : section.number,
            "label" : section.label,
            "sortOrder" : section.sort_order,
            "chapterId" : section.chapter_id,
            "updatedAt" : section.updated_at,
            "contentPath" : section.content_path,
            "startPage" : section.start_page,
            "endPage" : section.end_page,
            "reviewerAccess" : section.reviewer_access,
            "standards" : stds,
            "itemTypeId" : section.item_type_id,
            "teacherOnly" : section.teacher_only
          };
          let jsonStr = JSON.stringify(json);
          stream.write(jsonStr + "\n");
        }
        stream.end();
        console.log("sections: " + rows.length);
      });
    } else {
      console.log('Error fetching sections');
    }
  });

  let subsectionQuery = `
    SELECT ss.*, it.description item_type, it.level item_type_level
    FROM subsection ss
    JOIN item_type it ON ss.item_type_id = it.id
    JOIN sections s ON ss.section_id = s.id
    JOIN chapters c ON s.chapter_id = c.id
    JOIN book b ON c.book_id = b.id
    WHERE b.book_type = 2 and b.location_id IN ` + locations;
  conn.query(subsectionQuery, function(err, rows, fields) {
    if (!err) {
      let stream = fs.createWriteStream(config.dumpPath + "/subsections.json");
      stream.once('open', function(fd) {
        for (let i = 0; i < rows.length; i++) {
          let subsection = rows[i];
          let json = {
            "_id" : subsection.id,
            "sortOrder" : subsection.sort_order,
            "sectionId" : subsection.section_id,
            "itemTypeId" : subsection.item_type_id,
            "updatedAt" : subsection.updated_at,
            "teacherOnly" : subsection.teacher_only,
            "itemType" : subsection.item_type,
            "itemTypeLevel" : subsection.item_type_level,
			"title": subsection.title
          };
          let jsonStr = JSON.stringify(json);
          stream.write(jsonStr + "\n");
        }
        stream.end();
        console.log("subsections: " + rows.length);
      });
    } else {
      console.log('Error fetching subsections');
    }
  });

  let pageQuery = `
	SELECT * FROM page WHERE parent_type_id = 1 AND parent_id IN (
      SELECT b.id
      FROM book b 
      WHERE b.book_type = 2 and b.location_id IN ` + locations + `
	  and page.status in ` + statuses + `)
    UNION  
    SELECT * FROM page WHERE parent_type_id = 2 AND parent_id IN (
      SELECT c.id
      FROM chapters c
      JOIN book b ON c.book_id = b.id
      WHERE b.book_type = 2 and b.location_id IN ` + locations + `
	  and page.status in ` + statuses + `)
    UNION
    SELECT * FROM page WHERE parent_type_id = 3 AND parent_id IN (
      SELECT s.id
      FROM sections s
      JOIN chapters c ON s.chapter_id = c.id
      JOIN book b ON c.book_id = b.id
      WHERE b.book_type = 2 and b.location_id IN ` + locations + `
	  and page.status in ` + statuses + `)
    UNION
    SELECT * FROM page WHERE parent_type_id = 4 AND parent_id IN (
      SELECT ss.id
      FROM subsection ss
      JOIN sections s ON ss.section_id = s.id
      JOIN chapters c ON s.chapter_id = c.id
      JOIN book b ON c.book_id = b.id
      WHERE b.book_type = 2 and b.location_id IN ` + locations + `
	  and page.status in ` + statuses + `)
  `;
  conn.query(pageQuery, function(err, rows, fields) {
    if (!err) {
      let stream = fs.createWriteStream(config.dumpPath + "/pages.json");
      stream.once('open', function(fd) {
        for (let i = 0; i < rows.length; i++) {
          let page = rows[i];
          let json = {
            "_id" : page.id,
            "parentId" : page.parent_id,
            "parentTypeId" : page.parent_type_id,
            "sortOrder" : page.sort_order,
            "status" : page.status,
            "updatedAt" : page.updated_at,
            "teacherOnly" : page.teacher_only,
            "title" : page.title
          };
          let jsonStr = JSON.stringify(json);
          stream.write(jsonStr + "\n");
        }
        stream.end();
        console.log("pages: " + rows.length);
      });
    } else {
      console.log('Error fetching pages');
    }
  });

  let elementMap = {};

  let elementQuery = `
    SELECT p.id page_id, pe.element_id, pe.sort_order, e.element_type_id, et.description element_type,
    e.element_title, e.updated_at, e.teacher_only
    FROM page p JOIN page_element pe ON p.id = pe.page_id JOIN element e ON e.id = pe.element_id
    JOIN element_type et ON e.element_type_id = et.id
    ORDER BY p.id, pe.sort_order
  `;
  conn.query(elementQuery, function(err, rows, fields) {
    if (!err) {
      for (let i = 0; i < rows.length; i++) {
        let data = rows[i];
        let element = {
          "_id" : data.element_id,
          "elementId" : data.element_id,
          "pageId" : data.page_id,
          "sortOrder" : data.sort_order,
          "typeId" : data.element_type_id,
          "type" : data.element_type,
          "title" : data.element_title,
          "updatedAt" : data.updated_at,
          "teacherOnly" : data.teacher_only,
          "attributes" : [],
          "callouts" : []
        };
        elementMap[data.element_id] = element;
      }
      console.log("elements: " + rows.length);

      let attrConn = mysql.createConnection({
          host: config.host,
          port: config.port,
          user: config.user,
          password: config.password,
          database: config.database,
          typeCast: castFields
      });
      let attributeQuery = `
		SELECT p.id page_id, ea.element_id, ea.id, ea.attribute_id type_id, a.description type, ea.attribute_value value, etat.sort sort
        FROM page p JOIN page_element pe ON p.id = pe.page_id JOIN element e ON e.id = pe.element_id
        JOIN element_attribute ea ON e.id = ea.element_id JOIN attribute a ON a.id = ea.attribute_id
		JOIN element_type_attribute_template etat ON etat.element_type_id = e.element_type_id AND etat.attribute_id = ea.attribute_id
        order by page_id, ea.element_id, sort
      `;
      attrConn.query(attributeQuery, function(err, rows, fields) {
        if (!err) {
          for (let i = 0; i < rows.length; i++) {
            let data = rows[i];
            let element = elementMap[data.element_id];

            let attribute = {
              "id" : data.id,
              "typeId" : data.type_id,
              "type" : data.type,
              "value" : data.value,
			  "sort" : data.sort
            };
            element.attributes.push(attribute);
          }
          console.log("attributes: " + rows.length);

          let calloutConn = mysql.createConnection({
            host: config.host,
            port: config.port,
            user: config.user,
            password: config.password,
            database: config.database,
            typeCast: castFields
          });
          let calloutQuery = `
            SELECT p.id page_id, ec.element_id, ec.id, ec.callout_type_id type_id, ct.description type, ec.callout_value value,
              ec.sort_order, ec.teacher_only, ec.callout_title title
            FROM page p JOIN page_element pe ON p.id = pe.page_id JOIN element e ON e.id = pe.element_id
            JOIN element_callout ec ON e.id = ec.element_id JOIN callout_type ct ON ct.id = ec.callout_type_id
            ORDER BY ec.element_id, ec.sort_order
          `;
          calloutConn.query(calloutQuery, function(err, rows, fields) {
            if (!err) {
              for (let i = 0; i < rows.length; i++) {
                let data = rows[i];
                let element = elementMap[data.element_id];
                let callout = {
                  "id" : data.id,
                  "typeId" : data.type_id,
                  "type" : data.type,
                  "value" : data.value,
                  "sortOrder" : data.sort_order,
                  "teacherOnly" : data.teacher_only,
                  "title" : data.title
                };
                element.callouts.push(callout);
              }

              // All elements, attributes, and callouts have been fetched now so write out the file.
              let stream = fs.createWriteStream(config.dumpPath + "/elements.json");
              stream.once('open', function(fd) {
                for (key in elementMap) {
                  let element = elementMap[key];
                  let jsonStr = JSON.stringify(element);
                  stream.write(jsonStr + "\n");
                }
                stream.end();
              });
              console.log("callouts: " + rows.length);

            } else {
              console.log('Error fetching callouts', err);
            }
          });

          calloutConn.end();
        } else {
          console.log('Error fetching attributes', err);
        }
      });

      attrConn.end();
    } else {
      console.log('Error fetching elements');
    }
  });

  let learningTargetMap = {};

  let learningTargetQuery = `
    SELECT lt.id, lt.learning_target, lt.parent_id, lt.parent_type_id
    FROM learning_target lt
  `;
  conn.query(learningTargetQuery, function(err, rows, fields) {
    if (!err) {
      for (let i = 0; i < rows.length; i++) {
        let data = rows[i];
        let learningTarget = {
          "_id" : data.id,
          "learningTarget" : data.learning_target,
          "parentId" : data.parent_id,
          "parentTypeId" : data.parent_type_id,
          "successCriteria" : []
        };
        learningTargetMap[data.id] = learningTarget;
      }
      console.log("learning_targets: " + rows.length);

      let successConn = mysql.createConnection({
          host: config.host,
          port: config.port,
          user: config.user,
          password: config.password,
          database: config.database,
          typeCast: castFields
      });
      let successCriteriaQuery = `
        SELECT sc.id, sc.success_criteria, sc.learning_target_id, sc.level
        FROM success_criteria sc
      `;
      successConn.query(successCriteriaQuery, function(err, rows, fields) {
        if (!err) {
          for (let i = 0; i < rows.length; i++) {
            let data = rows[i];
            let learningTarget = learningTargetMap[data.learning_target_id];
//            console.log(learningTarget);

            if (learningTarget) {
              let successCriterion = {
                "id" : data.id,
                "successCriterion" : data.success_criteria,
                "level" : data.level
              };
//              console.log(successCriterion);
              learningTarget.successCriteria.push(successCriterion);
            }
          }
          console.log("success_criteria: " + rows.length);

          let stream = fs.createWriteStream(config.dumpPath + "/learningtargets.json");
          stream.once('open', function(fd) {
            for (key in learningTargetMap) {
              let learningTarget = learningTargetMap[key];
              let jsonStr = JSON.stringify(learningTarget);
              stream.write(jsonStr + "\n");
            }
            stream.end();
          });
        } else {
          console.log('Error fetching success_criteria', err);
        }
      });

      successConn.end();
    } else {
      console.log('Error fetching learning_targets');
    }
  });

  console.log("closing mysql connection");
  conn.end();
});